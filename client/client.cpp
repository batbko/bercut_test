#include <boost/asio.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>
#include <iostream>

namespace asio = boost::asio;

int main_loop(const std::string& host, uint16_t port) {
  asio::io_service io_service;
  asio::ip::tcp::socket socket(io_service);

  try {
    socket.connect(
        asio::ip::tcp::endpoint(asio::ip::address::from_string(host), port));

    for (std::string line; std::getline(std::cin, line);) {
      const std::string msg = line + "\n";
      asio::write(socket, asio::buffer(msg));

      asio::streambuf receive_buffer;
      asio::read_until(socket, receive_buffer, '\n');

      // quite ugly way to determine protocol message type
      const char* data = asio::buffer_cast<const char*>(receive_buffer.data());
      std::string as_string = data;
      as_string.pop_back();
      try {
        boost::lexical_cast<double>(as_string);
      } catch (const boost::bad_lexical_cast& e) {
        std::cerr << data;
        return 1;
      }
      std::cout << data;
    }
  } catch (const boost::system::system_error& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }

  return 0;
}

int main(int argc, char* argv[]) {
  namespace po = boost::program_options;

  try {
    po::options_description options("Allowed options");
    options.add_options()("help", "produce help message")(
        "host", po::value<std::string>()->default_value("127.0.0.1"),
        "bercut server's host name")("port",
                                     po::value<uint16_t>()->default_value(2222),
                                     "bercut server's port");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, options), vm);
    po::notify(vm);

    if (vm.count("help")) {
      std::cout << options << "\n";
      return 0;
    }

    return main_loop(vm["host"].as<std::string>(), vm["port"].as<uint16_t>());
  } catch (std::exception& e) {
    std::cerr << "error: " << e.what() << "\n";
  } catch (...) {
    std::cerr << "Exception of unknown type!\n";
  }
  return 1;
}
