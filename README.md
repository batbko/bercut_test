# What's this

This is a dirty implementation of client/server with telnet-like calculator as a test task for Bercut company job interview.


There are following components:

* Calculator grammar resumable parser, shows some unit testing practices and bycicle driving development
* Naive server implementation based on boost::asio async tcp server example, shows nice compatibility with telnet or netcat
* Blocking client ugly implementation, shows how shouldn't be built event loop


## Why not the qi and/or other peg parsers?

Because it's interesting to check what can be achieved if backtracking avoided. At least DoS possibility is highly reduced with debugged implementation. Also it could be faster then splitting implementation with incoming buffer growth, while there is quite big difference between target parsing result size and text represention of the message containing it.

Or, may be I've just wanted to invent some bycicle, again...

Anyway I've couldn't find ready to use parser which had has possibility of processing stream without growth of temporary buffer in dependency from message size + message complexity. And yep, I've realized in process, that this implementation has buffer growing in dependency of message complexity, but it's not growing in dependency of message size.


## Why clinet and server implementation is so poor?

* 90% of the Time wasted for parser implementation
* Hands
* TBD
