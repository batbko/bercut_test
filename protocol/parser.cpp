#include <parser.hpp>

#include <cctype>
#include <cmath>
#include <map>
#include <vector>

struct ParseResult {
    enum Result { Done, Wait };

    ParseResult() : result(Wait), value(NAN) {}
    ParseResult(Result result_, double value_ = NAN)
        : result(result_), value(value_) {}

    Result result;
    double value;
};

struct Context {
    using ParserFunctor = std::function<ParseResult(Context&)>;

    void push(ParserFunctor&& parser) { stack.push_back(std::move(parser)); }

    bool waiting() const { return stack.size(); }

    ParserFunctor pop() {
        auto p = std::move(stack.back());
        stack.pop_back();
        return p;
    }

    void skip() {
        while (!eof()) {
          char c = sniff();
            if ('\n' != c && isspace(c)) {
                next();
            } else {
                break;
            }
        }
    }

    void feed(ParserIt begin, ParserIt end) {
        it = begin;
        this->end = end;
    }

    bool eof() const { return it == end; }

    void error_unexpected() const { error("stub unexpected symbol"); }

    void error(const std::string&& message) const {
        throw ParserError(std::move(message));
    }

    char eat(const char* c) {
        do {
            if (*c == *it) {
                it++;
                return *c;
            }
        } while (*c++);

        error("stub");
        return '\0';
    }

    char next() { return *it++; }

    char sniff() const { return *it; }


    ParserIt it;
    ParserIt end;

    std::vector<ParserFunctor> stack;
};

// grammar:
// message :== <expression>\\n
// expression :== <additive>  { <sum_operator> <additive> }
// additive :== <multitive> { <mul_operator> <multitive> }
// multitive :== <number> | ( <expression )
// number :== [0-9]+\(.[0-9]+)?
// sum_operator :== +|-
// mul_operator :== *|/
class Parser {
   public:
    virtual ~Parser() = default;

    ParseResult consume(Context& ctx) {
        ctx.skip();
        if (ctx.eof()) {
            return ParseResult(ParseResult::Wait);
        }

        return consume_impl(ctx);
    }

    template <typename ParserT>
    static ParseResult apply_parser(Context& ctx) {
        Context::ParserFunctor parser;
        if (ctx.waiting()) {
            parser = std::move(ctx.pop());

        } else {
            ParserT specific_parser;
            parser = [specific_parser](Context& ctx) mutable -> ParseResult {
                return specific_parser.consume(ctx);
            };
        }

        auto result = parser(ctx);

        if (ParseResult::Wait == result.result) {
            ctx.push(std::move(parser));
        }
        return result;
    }

   protected:
    virtual ParseResult consume_impl(Context& ctx) = 0;
};

class NumberParser : public Parser {
   protected:
    ParseResult consume_impl(Context& ctx) override;

   private:
    std::vector<char> data;
    bool dot_seen = false;
};



template <typename LhsT, char op1, char op2, typename RhsT = LhsT>
// expression :== <additive>  { <sum_operator> <additive> }
class BinaryParser : public Parser {

      protected:
       using ThisT = BinaryParser<LhsT, op1, op2, RhsT>;

       virtual double op1_call(Context& ctx, double rhs, double lhs) = 0;
       virtual double op2_call(Context& ctx, double rhs, double lhs) = 0;

       ParseResult consume_impl(Context& ctx) override {
           using namespace std::placeholders;
           stages = {
               {Start, std::bind(&ThisT::eatLhs, this, _1)},
               {LhsCompleted, std::bind(&ThisT::eatOp, this, _1)},
               {OpSeen, std::bind(&ThisT::eatRhs, this, _1)}};

           while (!ctx.eof() && End != stage) {
               auto it = stages.find(stage);

               if (stages.end() == it) {
                   ctx.error("stub unexpected stage");
               }

               if (it->second(ctx)) {
                   continue;
               }

               break;
           }

           return result;
    }

    bool eatLhs(Context& ctx) {
        result = apply_parser<LhsT>(ctx);

        if (ParseResult::Wait == result.result) {
            return false;
        }

        stage = LhsCompleted;
        value = result.value;
        return true;
    }

    bool eatOp(Context& ctx) {
        ctx.skip();

        if (ctx.eof()) {
            return false;
        }

        char c = ctx.sniff();
        if ( op1 == c || op2 == c) {
            op = c;
            ctx.next();
            stage = OpSeen;
        } else {
            stage = End;
        }

        return true;
    }

    bool eatRhs(Context& ctx) {
            ctx.skip();
            result = apply_parser<RhsT>(ctx);

            if (ParseResult::Wait == result.result) {
                return false;
            }

            if (op1 == op) {
                value = op1_call(ctx, value, result.value);
            } else {
                value = op2_call(ctx, value, result.value);
            }

            result.value = value;
            stage = LhsCompleted;

            return true;
    }

    enum Stage { Start, LhsCompleted, OpSeen, End };
    using StageCall = std::function<bool(Context&)>;
    std::map<Stage, StageCall> stages;
    ParseResult result;
    Stage stage = Start;
    double value = NAN;
    char op = '\0';
    
};

// multitive :== <number> | ( <expression )
class MultitiveParser : public Parser {
   protected:
    ParseResult consume_impl(Context& ctx) override;
    enum Stage { Start, OpenBracketSeen, ExpressionSeen};
    double value = NAN;
    Stage stage = Start;
};

// additive :== <multitive> { <mul_operator> <multitive> }
class AdditiveParser : public BinaryParser<MultitiveParser, '/', '*'> {
   protected:
    double op1_call(Context& ctx, double lhs, double rhs) override {
        if (FP_ZERO == std::fpclassify(result.value)) {
            ctx.error("stub divizion by zero");
        }
        return lhs / rhs;
    }
    double op2_call(Context&, double lhs, double rhs) override {
        return lhs * rhs;
    }
};

// expression :== <additive>  { <sum_operator> <additive> }
class ExpressionParser : public BinaryParser<AdditiveParser, '-', '+'> {
   protected:
    double op1_call(Context&, double lhs, double rhs) override {
        return lhs - rhs;
    }
    double op2_call(Context&, double lhs, double rhs) override {
        return lhs + rhs;
    }
};

ParseResult MultitiveParser::consume_impl(Context& ctx) {
    if (Start == stage) {
        if ('(' != ctx.sniff()) {
            return apply_parser<NumberParser>(ctx);
        } else {
            ctx.next();
            ctx.skip();
            stage = OpenBracketSeen;
        }
    }

    if (OpenBracketSeen == stage) {
        auto result = apply_parser<ExpressionParser>(ctx);

        if (ParseResult::Wait == result.result) {
            return result;
        }

        value = result.value;
        stage = ExpressionSeen;
    }

    ctx.skip();

    if (ctx.eof()) {
        return ParseResult(ParseResult::Wait);
    }

    ctx.eat(")");

    return ParseResult(ParseResult::Done, value);
}


class MessageParser : public Parser {
   protected:
    ParseResult consume_impl(Context& ctx) override {
        if (waitOrEatValue(ctx) )
            return result;

        ctx.skip();
        if (ctx.eof()) { 
            return ParseResult::Wait;
        }

        ctx.eat("\n");
        return result;
    }

    bool waitOrEatValue(Context& ctx) {
        if (!std::isnan(value)) {
            return false;
        }

        result = apply_parser<ExpressionParser>(ctx);

        if (ParseResult::Wait == result.result) {
            return true;
        }

        value = result.value;

        return false;
    }

    double value = NAN;
    ParseResult result;
};


ParseResult NumberParser::consume_impl(Context& ctx) {
  while (true) {
    if (ctx.eof()) {
      return ParseResult(ParseResult::Wait);
    }

    auto c = ctx.sniff();
    if (!isdigit(c)) {
      if (!data.size() || '.' == data.back()) {
        ctx.error("stub digit expected");
      } else if (c == '.') {
        if (dot_seen || !data.size()) {
          ctx.error_unexpected();
        } else {
          dot_seen = true;
        }
      } else {
        break;
      }
    }

    ctx.next();
    data.push_back(c);
  }

  data.push_back('\0');
  return ParseResult(ParseResult::Done, std::atof(data.data()));
}

ProtocolParser::ProtocolParser()
    : context(std::move(std::make_unique<Context>())) {}

ProtocolParser::~ProtocolParser() {}

void ProtocolParser::feed(
    ParserIt begin, ParserIt end, StreamParser::ResultHandler handler) {
    context->feed(begin, end);
    while (!context->eof()) {
        auto result = Parser::apply_parser<MessageParser>(*context);
        if (ParseResult::Done == result.result) {
            handler(result.value);
        }
    }
}
