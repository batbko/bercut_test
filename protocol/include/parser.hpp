#pragma once

#include <functional>
#include <memory>
#include <utility>
#include <stdexcept>


using ParserIt = const char*;

class StreamParser {
 public:
  using ResultHandler = std::function<void (double)>;
  virtual ~StreamParser() = default;
  virtual void feed(ParserIt begin, ParserIt end, ResultHandler handler) = 0;
};

struct ParserError : std::runtime_error {
 public:
  using runtime_error::runtime_error;
};

struct Context;

class ProtocolParser : public StreamParser {
 public:
  ProtocolParser();
  ~ProtocolParser();
  ProtocolParser(const ProtocolParser& other) = delete;
  void feed(ParserIt begin, ParserIt end, ResultHandler handler) override;

 private:
  std::unique_ptr<Context> context;
};
