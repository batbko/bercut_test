#include <memory>
#include <string>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "parser.hpp"

using testing::_;
using testing::MockFunction;

struct ProtocolParserSmoke : ::testing::Test {
   protected:
    void SetUp() override { parser = std::make_unique<ProtocolParser>(); }

    void TearDown() override {
        ::testing::Mock::VerifyAndClearExpectations(&resultHandlerMock);
    }

    std::unique_ptr<ProtocolParser> parser;
    MockFunction<void(double)> resultHandlerMock;
};

TEST_F(ProtocolParserSmoke, positive_smoke) {
    std::string number = "123\n";
    EXPECT_CALL(resultHandlerMock, Call(123)).Times(1);
    parser->feed(&*number.begin(), &*number.end(),
                 resultHandlerMock.AsStdFunction());
}

TEST_F(ProtocolParserSmoke, positive_with_dot_smoke) {
    std::string number = "12.3\n";
    EXPECT_CALL(resultHandlerMock, Call(12.3)).Times(1);
    parser->feed(&*number.begin(), &*number.end(),
                 resultHandlerMock.AsStdFunction());
}

TEST_F(ProtocolParserSmoke, negative_not_a_digit) {
    std::string number = "a12.3";
    ASSERT_THROW(parser->feed(&*number.begin(), &*number.end(),
                              resultHandlerMock.AsStdFunction()),
                 ParserError);
}

TEST_F(ProtocolParserSmoke, negative_2dots) {
    std::string number = "12.3.4";
    ASSERT_THROW(parser->feed(&*number.begin(), &*number.end(),
                              resultHandlerMock.AsStdFunction()),
                 ParserError);
}

TEST_F(ProtocolParserSmoke, negative_starts_with_dot) {
    std::string number = ".12\n";
    ASSERT_THROW(parser->feed(&*number.begin(), &*number.end(),
                              resultHandlerMock.AsStdFunction()),
                 ParserError);
}

TEST_F(ProtocolParserSmoke, test_empty) {
    std::string test;
    EXPECT_CALL(resultHandlerMock, Call(_)).Times(0);
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
}

TEST_F(ProtocolParserSmoke, test_minimum_valid) {
    std::string test = "3\n";
    EXPECT_CALL(resultHandlerMock, Call(3)).Times(1);
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
}

TEST_F(ProtocolParserSmoke, test_eats_stucked) {
    std::string test = "3\n2\n1\n";
    EXPECT_CALL(resultHandlerMock, Call(3)).Times(1);
    EXPECT_CALL(resultHandlerMock, Call(2)).Times(1);
    EXPECT_CALL(resultHandlerMock, Call(1)).Times(1);

    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
}

TEST_F(ProtocolParserSmoke, test_eats_rest) {
    std::string test = "3 \n 3";
    EXPECT_CALL(resultHandlerMock, Call(3)).Times(1);
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
    test = "\n";
    EXPECT_CALL(resultHandlerMock, Call(3)).Times(1);
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
}

TEST_F(ProtocolParserSmoke, test_eats_numbers_rest) {
    std::string test = "1.";
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
    test = "22\n";
    EXPECT_CALL(resultHandlerMock, Call(1.22)).Times(1);
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
}

TEST_F(ProtocolParserSmoke, test_sum) {
    std::string test = "22+22\n 1.22 - 42 \n";
    EXPECT_CALL(resultHandlerMock, Call(22 + 22)).Times(1);
    EXPECT_CALL(resultHandlerMock, Call(1.22 - 42)).Times(1);
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
}

TEST_F(ProtocolParserSmoke, test_sum_complex) {
    std::string test = "1.1 + 2.2 - 3.3 + 4.4\n";
    EXPECT_CALL(resultHandlerMock, Call(1.1 + 2.2 - 3.3 + 4.4)).Times(1);
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
}

TEST_F(ProtocolParserSmoke, test_sum_feed_at_number_dot_before) {
    std::string test = "1.1 + 2.";
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
    EXPECT_CALL(resultHandlerMock, Call(1.1 + 2.2)).Times(1);
    test = "2\n";
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
}

TEST_F(ProtocolParserSmoke, test_sum_feed_at_number_dot_after) {
    std::string test = "1.1 + 2";
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
    test = ".2\n";
    EXPECT_CALL(resultHandlerMock, Call(1.1 + 2.2)).Times(1);
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
}

TEST_F(ProtocolParserSmoke, test_sum_feed_at_number) {
    std::string test = "1.1 + 2";
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
    test = "2\n";
    EXPECT_CALL(resultHandlerMock, Call(1.1 + 22)).Times(1);
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
}

TEST_F(ProtocolParserSmoke, test_sum_feed_at_operator) {
    std::string test = "1.1 + 2.2 - ";
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
    test = "3.3 + 4.4\n";
    EXPECT_CALL(resultHandlerMock, Call(1.1 + 2.2 - 3.3 + 4.4)).Times(1);
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
}

TEST_F(ProtocolParserSmoke, test_mul_negative) {
    std::string test = "22*\n";
    ASSERT_THROW(parser->feed(&*test.begin(), &*test.end(),
                              resultHandlerMock.AsStdFunction()),
                 ParserError);
    test = "*22\n";
    ASSERT_THROW(parser->feed(&*test.begin(), &*test.end(),
                              resultHandlerMock.AsStdFunction()),
                 ParserError);
}

TEST_F(ProtocolParserSmoke, test_mul) {
    std::string test = "22*22\n 1.22 / 42 \n";
    EXPECT_CALL(resultHandlerMock, Call(22 * 22)).Times(1);
    EXPECT_CALL(resultHandlerMock, Call(1.22 / 42)).Times(1);
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
}

TEST_F(ProtocolParserSmoke, test_division_by_zero) {
    std::string test = "1 / 0\n";
    ASSERT_THROW(parser->feed(&*test.begin(), &*test.end(),
                              resultHandlerMock.AsStdFunction()),
                 ParserError);
}

TEST_F(ProtocolParserSmoke, test_mul_complex) {
    std::string test = "1.1 * 2.2 / 3.3 * 4.4\n";
    EXPECT_CALL(resultHandlerMock, Call(1.1 * 2.2 / 3.3 * 4.4)).Times(1);
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
}

TEST_F(ProtocolParserSmoke, test_mul_feed_at_operator) {
    std::string test = "1.1 * 2.2 / ";
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
    test = "3.3 * 4.4\n";
    EXPECT_CALL(resultHandlerMock, Call(1.1 * 2.2 / 3.3 * 4.4)).Times(1);
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
}

TEST_F(ProtocolParserSmoke, test_mul_feed_at_number) {
    std::string test = "1.1 * 2.2 / 3";
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
    test = ".3 * 4.4\n";
    EXPECT_CALL(resultHandlerMock, Call(1.1 * 2.2 / 3.3 * 4.4)).Times(1);
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
}

TEST_F(ProtocolParserSmoke, test_expr_negative) {
    std::string test = "(\n";
    ASSERT_THROW(parser->feed(&*test.begin(), &*test.end(),
                              resultHandlerMock.AsStdFunction()),
                 ParserError);
    test = "(42+1\n";
    ASSERT_THROW(parser->feed(&*test.begin(), &*test.end(),
                              resultHandlerMock.AsStdFunction()),
                 ParserError);
    test = "42+1)\n";
    ASSERT_THROW(parser->feed(&*test.begin(), &*test.end(),
                              resultHandlerMock.AsStdFunction()),
                 ParserError);
}

TEST_F(ProtocolParserSmoke, test_expr) {
    std::string test = "(42 + 42) * 42\n";
    EXPECT_CALL(resultHandlerMock, Call((42 + 42) * 42)).Times(1);
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
}

TEST_F(ProtocolParserSmoke, test_expr_complex) {
    std::string test = "(1 + (2) / ( 3 / 4)) * ((5) + 6)\n";
    EXPECT_CALL(resultHandlerMock,
                Call((1.0 + (2.0) / (3.0 / 4.0)) * ((5.0) + 6.0)))
        .Times(1);
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
}

TEST_F(ProtocolParserSmoke, test_expr_feed_at_open_bracket) {
    std::string test = "(";
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
    test = "1.1 * 2.2)\n";
    EXPECT_CALL(resultHandlerMock, Call(1.1 * 2.2)).Times(1);
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
}

TEST_F(ProtocolParserSmoke, test_expr_feed_at_close_bracket) {
    std::string test = "(1.1 * 2.2";
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
    test = ")\n";
    EXPECT_CALL(resultHandlerMock, Call(1.1 * 2.2)).Times(1);
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
}

TEST_F(ProtocolParserSmoke, test_expr_feed_at_content) {
    std::string test = "(1.1 * 2";
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
    test = ".2)\n";
    EXPECT_CALL(resultHandlerMock, Call(1.1 * 2.2)).Times(1);
    parser->feed(&*test.begin(), &*test.end(),
                 resultHandlerMock.AsStdFunction());
}
