#include <map>
#include <string>
#include <thread>

#include <parser.hpp>
//
// async_tcp_echo_server.cpp
// ~~~~~~~~~~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2020 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <cstdlib>
#include <iostream>

using boost::asio::ip::tcp;

class session {
 public:
  session(boost::asio::io_service& io_service) : socket_(io_service) {}

  tcp::socket& socket() { return socket_; }

  void start() {
    socket_.async_read_some(
        boost::asio::buffer(data_, max_length),
        boost::bind(&session::handle_read, this,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred));
  }

 private:
  void schedule_read_chunk() {
    socket_.async_read_some(
        boost::asio::buffer(data_, max_length),
        boost::bind(&session::handle_read, this,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred));
  }

  static std::string error_reply(const std::string& prefix,
                                 const std::string& what = std::string()) {
    return prefix + what + "\n";
  };

  void handle_read(const boost::system::error_code& error,
                   size_t bytes_transferred) {
    if (!error) {
      std::string reply;
      error_state = true;

      try {
        parser.feed(data_, data_ + bytes_transferred, [&reply](double result) {
          reply += std::to_string(result);
          reply += "\n";
        });

        if (0 == reply.size()) {
          schedule_read_chunk();
          return;
        }

        error_state = false;
      } catch (ParserError& error) {
        reply += error_reply("ParserError:", error.what());
      } catch (std::exception& error) {
        reply += error_reply("Unexpected error:", error.what());
      } catch (...) {
        reply += error_reply("Unexpected error");
      }

      boost::asio::async_write(socket_,
                               boost::asio::buffer(reply.data(), reply.size()),
                               boost::bind(&session::handle_write, this,
                                           boost::asio::placeholders::error));
    } else {
      delete this;
    }
  }

  void handle_write(const boost::system::error_code& error) {
    if (!error && !error_state) {
      schedule_read_chunk();
    } else {
      delete this;
    }
  }

  // threre are no synchronization required for current state machine while
  // only one I/O operation is scheduled at any step for now
  tcp::socket socket_;
  enum { max_length = 1024 };
  char data_[max_length];
  ProtocolParser parser;
  bool error_state = true;
};

class server {
 public:
  server(boost::asio::io_service& io_service, short port)
      : io_service_(io_service),
        acceptor_(io_service, tcp::endpoint(tcp::v4(), port)) {
    start_accept();
  }

 private:
  void start_accept() {
    session* new_session = new session(io_service_);
    acceptor_.async_accept(
        new_session->socket(),
        boost::bind(&server::handle_accept, this, new_session,
                    boost::asio::placeholders::error));
  }

  void handle_accept(session* new_session,
                     const boost::system::error_code& error) {
    if (!error) {
      new_session->start();
    } else {
      delete new_session;
    }

    start_accept();
  }

  boost::asio::io_service& io_service_;
  tcp::acceptor acceptor_;
};

int main(int argc, char* argv[]) {
  try {
    if (argc != 2) {
      std::cerr << "Usage: server <port>\n";
      return 1;
    }

    boost::asio::io_service io_service;
    std::vector<std::thread> pool;

    server s(io_service, std::atoi(argv[1]));

    const auto concurrency = std::thread::hardware_concurrency() + 1;

    // single threading server is good enough for achieving requirements,
    // while this is ~twice better under load in dirty tests with timed nc
    // instances in parallel
    for (auto i = 0u; i < concurrency; ++i) {
      pool.emplace_back([&io_service]() { io_service.run(); });
    }

    for (auto& t : pool) {
      t.join();
    }
  } catch (std::exception& e) {
    std::cerr << "Exception: " << e.what() << "\n";
  }

  return 0;
}
